var jsHelpers = require('origami-js-function-helpers');

function addApi(socket, token, apiName, methods, api, notify) {
  var ShadowClient = jsHelpers.createApi(
    apiName,
    methods,
    function (apiName, methodName, argNames) {
      return jsHelpers.createMethod(
        methodName,
        argNames,
        function () {
          var argsObject = {};

          for (var i = 0; i < arguments.length; i++) {
            argsObject[argNames[i]] = arguments[i];
          }

          return new Promise(function (resolve, reject) {
            socket
            .emit(
              'api',
              token,
              apiName,
              methodName,
              argsObject,
              function (err, result) {
                if (err) {
                  reject(err);
                  
                  if (notify) notify();
                  
                  return;
                } 

                resolve(result);
                  
                if (notify) notify();
              }
            );
          });
        }
      );
    }
  );

  api[apiName] = Object.create(ShadowClient.prototype);
  ShadowClient.apply(api[apiName]);
}

function Client (socket, apis, token, notifications) {
  if (!socket) throw new Error('socket is required');
  if (!apis) throw new Error('apis are required');

  var api = {};

  for (var apiName in apis) {
    addApi(socket, token, apiName, apis[apiName], api, notifications);
  }
  
  return api;
}

module.exports = Client;

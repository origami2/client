var Client = require('..');
var assert = require('assert');
var EventEmitter = require('events').EventEmitter;

describe('Client', function () {
  it('requires socket', function () {
    assert.throws(
      function () {
        new Client();
      },
      /socket is required/
    );
  });
  
  it('requires apis', function () {
    var fakeSocket = new EventEmitter();
    
    assert.throws(
      function () {
        new Client(fakeSocket);
      },
      /apis are required/
    );
  });
  
  it('builds object with methods', function () {
    var fakeSocket = new EventEmitter();
    
    var client = new Client(
      fakeSocket, 
      {
        'api1': {
          'method1': [ 'param1' ]
        }
      }
    );
    
    assert(client);
    assert(client.api1);
    assert(client.api1.method1);
  });
  
  it('sends #api through socket on method invocation', function (done) {
    var fakeSocket = new EventEmitter();
    
    var client = new Client(
      fakeSocket, 
      {
        'api1': {
          'method1': [ 'param1' ]
        }
      }
    );
    
    fakeSocket
    .on(
      'api',
      function (stackToken, apiName, methodName, params, callback) {
        try {
          assert(!stackToken);
          assert.equal('api1', apiName);
          assert.equal('method1', methodName);
          assert.deepEqual(
            {
              'param1': 'hello'
            },
            params
          );
          assert('function', typeof(callback));
          
          done();
        } catch (e) {
          done(e);
        }
      }
    );
    
    client.api1.method1('hello');
  });
  
  it('sends stack token along with #api', function (done) {
    var fakeSocket = new EventEmitter();
    var stackToken = {
      someData: 'abcd1234'
    };
    
    var client = new Client(
      fakeSocket, 
      {
        'api1': {
          'method1': [ 'param1' ]
        }
      },
      stackToken
    );
    
    fakeSocket
    .on(
      'api',
      function (receivedToken, apiName, methodName, params, callback) {
        try {
          assert.deepEqual(
            receivedToken,
            stackToken
          );
          
          done();
        } catch (e) {
          done(e);
        }
      }
    );
    
    client.api1.method1('hello');
  });
  
  it('resolves promise on successfull callback', function (done) {
    var fakeSocket = new EventEmitter();

    var client = new Client(
      fakeSocket, 
      {
        'api1': {
          'method1': [ 'param1' ]
        }
      }
    );
    
    fakeSocket
    .on(
      'api',
      function (stackToken, apiName, methodName, params, callback) {
        callback(null, 'abcd1234');
      }
    );
    
    client
    .api1
    .method1('hello')
    .then(function (result) {
      try {
        assert.equal('abcd1234', result);
        
        done();
      } catch (e) {
        done(e);
      }
    });
  });
  
  it('rejects promise on erroneous callback', function (done) {
    var fakeSocket = new EventEmitter();

    var client = new Client(
      fakeSocket, 
      {
        'api1': {
          'method1': [ 'param1' ]
        }
      }
    );
    
    fakeSocket
    .on(
      'api',
      function (stackToken, apiName, methodName, params, callback) {
        callback('abcd1234');
      }
    );
    
    client
    .api1
    .method1('hello')
    .catch(function (error) {
      try {
        assert.equal('abcd1234', error);
        
        done();
      } catch (e) {
        done(e);
      }
    });
  });
  
  it('invokes notify on successfull callback', function (done) {
    var fakeSocket = new EventEmitter();

    var client = new Client(
      fakeSocket, 
      {
        'api1': {
          'method1': [ 'param1' ]
        }
      },
      null,
      function () {
        done();
      }
    );
    
    fakeSocket
    .on(
      'api',
      function (stackToken, apiName, methodName, params, callback) {
        callback(null, 'abcd1234');
      }
    );
    
    client
    .api1
    .method1('hello');
  });
  
  it('invokes notify on erroneous callback', function (done) {
    var fakeSocket = new EventEmitter();

    var client = new Client(
      fakeSocket, 
      {
        'api1': {
          'method1': [ 'param1' ]
        }
      },
      null,
      function () {
        done();
      }
    );
    
    fakeSocket
    .on(
      'api',
      function (stackToken, apiName, methodName, params, callback) {
        callback('some error');
      }
    );

    client
    .api1
    .method1('hello');
  });
});
